from .base import *


SECRET_KEY = 't1i@=2s546#389vb!itf@!wcxga0r+f&p+-6wryjw)e)&_!!25'

DEBUG = True

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}