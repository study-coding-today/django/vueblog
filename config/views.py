from django.views.generic import TemplateView


# HomeView 테이블과 관계없고 view 만 보여줄 것으로 TemplateView 만 상속
class HomeView(TemplateView):
    template_name = 'home.html'
    