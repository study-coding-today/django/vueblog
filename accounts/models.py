from django.contrib.auth.models import AbstractUser
from django.db import models


# AbstractUser 를 사용하여 django 제공 User 테이블을 사용하며 컬럼을 추가하여 사용 가능하다.
# migration 위해 base.py 에 인증등록을 해야 한다. AUTH_USER_MODEL = 'accounts.User'
class User(AbstractUser):
    pass
