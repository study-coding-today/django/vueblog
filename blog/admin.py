from django.contrib import admin

from blog.models import Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'modify_dt', 'tag_list')

    # tag 데이터를 가져올때 prefetch_related method 를 사용하며 db 쿼리를 횟수를 줄이기 위해서
    # Post 의 레코드를 가져올때 tag 레코드도 함께 가져오라는 method 이다.
    # 두 table 이 N:N 일때는 prefetch_related() 를 1:N 일때는 select_related() 를 사용
    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related('tags')

    # tag_list 는 admin 에 보여줄 항목으로 method 로 작성한다.
    # obj,tags.all() db tag table 에 있는 데이터를 모두 가져와 ',' 로 합쳐준다.
    def tag_list(self, obj):
        return u", ".join(o.name for o in obj.tags.all())