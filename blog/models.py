from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from taggit.managers import TaggableManager


class Post(models.Model):
    title = models.CharField(verbose_name='TITLE', max_length=50)
    description = models.CharField('DESCRIPTION', max_length=100, blank=True, help_text='simple description text')
    content = models.TextField('CONTENT')
    create_dt = models.DateTimeField('CREATE DATE', auto_now_add=True)
    modify_dt = models.DateTimeField('MODIFY DATE', auto_now=True)
    # tag 를 사용하기 위해 pip install django-taggit 설치후 setting.py 에 taggit 을 등록한다.
    tags = TaggableManager(blank=True)
    # django 에서 지원하는 인증 테이블 모델 "get_user_model()" 를 사용
    # 1. import User 2. 'accounts.User' 3. settings.AUTH_USER_MODEL 4. get_user_model()
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, verbose_name='OWNER', blank=True, null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        # redirect 와 같이 역주소
        return reverse('blog:post_detail', args=(self.id,))

    def get_prev(self):
        # 위 modify_dt 를 기준으로 전 포스트를 반환
        return self.get_previous_by_modify_dt()

    def get_next(self):
        # 위 modify_dt 를 기준으로 다음 포스트를 반환
        return self.get_next_by_modify_dt()
