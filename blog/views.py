from django.shortcuts import render
from django.views.generic import ListView, DetailView

from blog.models import Post


class PostView(ListView):
    model = Post
    # 템플릿 이름은 default 이름과 같아 생략 가능하다.
    template_name = 'blog/post_list.html'


# 테이블에서 특정 레코드 하나만 가져올 것이므로 DetailView 상속
class PostDV(DetailView):
    model = Post
    # template_name = 'blog/post_detail.html'

